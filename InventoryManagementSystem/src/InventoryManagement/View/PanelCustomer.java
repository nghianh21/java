package InventoryManagement.View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.SystemColor;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;

public class PanelCustomer extends JPanel {
	private DefaultTableModel dtmTraCuu;
	private JTable tblTraCuu;
	private JTextField txtID;
	private JTextField txtDisplayName;
	private JTextField txtAddress;
	private JTextField txtPhone;
	private JTextField txtMail;
	/**
	 * Create the panel.
	 */
	public PanelCustomer() {
		setBackground(new Color(255, 255, 255));
		setLayout(new GridLayout(1, 2, 0, 0));

		JPanel pnInfo = new JPanel();
		pnInfo.setBackground(new Color(255, 255, 255));
		add(pnInfo);

		JButton btnThem = new JButton("Thêm mới");
		btnThem.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnThem.setForeground(SystemColor.text);
		btnThem.setBackground(new Color(30, 144, 255));

		JButton btnSua = new JButton("Cập nhật");
		btnSua.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnSua.setForeground(new Color(255, 255, 255));
		btnSua.setBackground(new Color(30, 144, 255));

		JButton btnXoa = new JButton("Xóa");
		btnXoa.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnXoa.setForeground(new Color(255, 255, 255));
		btnXoa.setBackground(new Color(205, 92, 92));

		txtID = new JTextField();
		txtID.setText("ID : CSD654");
		txtID.setEditable(false);
		txtID.setColumns(10);

		JLabel lblName = new JLabel("Tên khách hàng");
		lblName.setForeground(new Color(128, 128, 128));
		lblName.setFont(new Font("Tahoma", Font.BOLD, 13));

		txtDisplayName = new JTextField();
		txtDisplayName.setForeground(new Color(25, 25, 112));
		txtDisplayName.setFont(new Font("Arial", Font.PLAIN, 14));
		txtDisplayName.setColumns(10);

		JLabel lblAddress = new JLabel("Địa chỉ");
		lblAddress.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblAddress.setForeground(new Color(128, 128, 128));

		txtAddress = new JTextField();
		txtAddress.setFont(new Font("Arial", Font.PLAIN, 14));
		txtAddress.setForeground(new Color(25, 25, 112));
		txtAddress.setColumns(10);

		JLabel lblPhone = new JLabel("Số điện thoại");
		lblPhone.setForeground(new Color(128, 128, 128));
		lblPhone.setFont(new Font("Tahoma", Font.BOLD, 13));

		txtPhone = new JTextField();
		txtPhone.setForeground(new Color(25, 25, 112));
		txtPhone.setFont(new Font("Arial", Font.PLAIN, 14));
		txtPhone.setColumns(10);

		JLabel lblMail = new JLabel("Email");
		lblMail.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblMail.setForeground(new Color(128, 128, 128));

		txtMail = new JTextField();
		txtMail.setForeground(new Color(25, 25, 112));
		txtMail.setFont(new Font("Arial", Font.PLAIN, 14));
		txtMail.setColumns(10);
		GroupLayout gl_pnInfo = new GroupLayout(pnInfo);
		gl_pnInfo.setHorizontalGroup(
				gl_pnInfo.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, gl_pnInfo.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_pnInfo.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_pnInfo.createSequentialGroup()
										.addGroup(gl_pnInfo.createParallelGroup(Alignment.LEADING)
												.addComponent(txtID, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(lblName)
												.addComponent(lblAddress)
												.addGroup(gl_pnInfo.createSequentialGroup()
														.addGroup(gl_pnInfo.createParallelGroup(Alignment.LEADING)
																.addComponent(lblPhone)
																.addComponent(txtPhone, GroupLayout.DEFAULT_SIZE, 117, Short.MAX_VALUE))
														.addGap(18)
														.addGroup(gl_pnInfo.createParallelGroup(Alignment.LEADING)
																.addComponent(lblMail)
																.addComponent(txtMail, GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)))
												.addGroup(gl_pnInfo.createSequentialGroup()
														.addComponent(btnThem)
														.addGap(18)
														.addComponent(btnSua)
														.addPreferredGap(ComponentPlacement.RELATED, 72, Short.MAX_VALUE)
														.addComponent(btnXoa)))
										.addContainerGap())
								.addGroup(Alignment.TRAILING, gl_pnInfo.createSequentialGroup()
										.addGroup(gl_pnInfo.createParallelGroup(Alignment.TRAILING)
												.addComponent(txtDisplayName, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 331, Short.MAX_VALUE)
												.addComponent(txtAddress, GroupLayout.DEFAULT_SIZE, 331, Short.MAX_VALUE))
										.addContainerGap())))
				);
		gl_pnInfo.setVerticalGroup(
				gl_pnInfo.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnInfo.createSequentialGroup()
						.addContainerGap()
						.addComponent(txtID, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(lblName)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(txtDisplayName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(13)
						.addComponent(lblAddress)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(txtAddress, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(gl_pnInfo.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblPhone)
								.addComponent(lblMail))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_pnInfo.createParallelGroup(Alignment.BASELINE)
								.addComponent(txtPhone, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtMail, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(28)
						.addGroup(gl_pnInfo.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnThem)
								.addComponent(btnXoa)
								.addComponent(btnSua))
						.addContainerGap(177, Short.MAX_VALUE))
				);

		dtmTraCuu = new DefaultTableModel();
		dtmTraCuu.addColumn("STT");
		dtmTraCuu.addColumn("Tên khách hàng");

		pnInfo.setLayout(gl_pnInfo);

		JPanel pnTable = new JPanel();
		pnTable.setBackground(new Color(255, 255, 255));
		add(pnTable);
		pnTable.setLayout(new BorderLayout(0, 0));
		tblTraCuu = new JTable(dtmTraCuu);


		JScrollPane scrollTblTraCuu = new JScrollPane(tblTraCuu,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		pnTable.add(scrollTblTraCuu, BorderLayout.CENTER);
	}
}
