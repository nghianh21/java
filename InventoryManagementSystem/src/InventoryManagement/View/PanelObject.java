package InventoryManagement.View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.SystemColor;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.JComboBox;

public class PanelObject extends JPanel {
	private DefaultTableModel dtmTraCuu;
	private JTable tblTraCuu;
	private JTextField txtID;
	private JTextField txtDisplayName;
	/**
	 * Create the panel.
	 */
	public PanelObject() {
		setBackground(new Color(255, 255, 255));
		setLayout(new GridLayout(1, 2, 0, 0));

		JPanel pnInfo = new JPanel();
		pnInfo.setBackground(new Color(255, 255, 255));
		add(pnInfo);

		JButton btnThem = new JButton("Thêm mới");
		btnThem.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnThem.setForeground(SystemColor.text);
		btnThem.setBackground(new Color(30, 144, 255));

		JButton btnSua = new JButton("Cập nhật");
		btnSua.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnSua.setForeground(new Color(255, 255, 255));
		btnSua.setBackground(new Color(30, 144, 255));

		JButton btnXoa = new JButton("Xóa");
		btnXoa.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnXoa.setForeground(new Color(255, 255, 255));
		btnXoa.setBackground(new Color(205, 92, 92));

		txtID = new JTextField();
		txtID.setText("ID : CSD654");
		txtID.setColumns(10);

		JLabel lblName = new JLabel("Tên vật tư");
		lblName.setForeground(new Color(128, 128, 128));
		lblName.setFont(new Font("Tahoma", Font.BOLD, 13));

		txtDisplayName = new JTextField();
		txtDisplayName.setForeground(new Color(25, 25, 112));
		txtDisplayName.setFont(new Font("Arial", Font.PLAIN, 14));
		txtDisplayName.setColumns(10);

		JLabel lblAddress = new JLabel("Nhà cung cấp");
		lblAddress.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblAddress.setForeground(new Color(128, 128, 128));

		JLabel lblUnit = new JLabel("Đơn vị tính");
		lblUnit.setForeground(new Color(128, 128, 128));
		lblUnit.setFont(new Font("Tahoma", Font.BOLD, 13));
		
		JComboBox cbNhaCungCap = new JComboBox();
		cbNhaCungCap.setBackground(new Color(255, 255, 255));
		
		JComboBox cbDonViTinh = new JComboBox();
		cbDonViTinh.setBackground(Color.WHITE);
		GroupLayout gl_pnInfo = new GroupLayout(pnInfo);
		gl_pnInfo.setHorizontalGroup(
			gl_pnInfo.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnInfo.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_pnInfo.createParallelGroup(Alignment.LEADING)
						.addComponent(txtID, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblName)
						.addComponent(txtDisplayName, GroupLayout.DEFAULT_SIZE, 382, Short.MAX_VALUE)
						.addGroup(gl_pnInfo.createSequentialGroup()
							.addComponent(btnThem)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(btnSua)
							.addPreferredGap(ComponentPlacement.RELATED, 129, Short.MAX_VALUE)
							.addComponent(btnXoa))
						.addGroup(gl_pnInfo.createSequentialGroup()
							.addGroup(gl_pnInfo.createParallelGroup(Alignment.LEADING)
								.addComponent(lblAddress)
								.addComponent(lblUnit))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(gl_pnInfo.createParallelGroup(Alignment.LEADING)
								.addComponent(cbDonViTinh, GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE)
								.addComponent(cbNhaCungCap, 0, 284, Short.MAX_VALUE))))
					.addContainerGap())
		);
		gl_pnInfo.setVerticalGroup(
			gl_pnInfo.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnInfo.createSequentialGroup()
					.addContainerGap()
					.addComponent(txtID, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblName)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(txtDisplayName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(13)
					.addGroup(gl_pnInfo.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAddress)
						.addComponent(cbNhaCungCap, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_pnInfo.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblUnit)
						.addComponent(cbDonViTinh, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_pnInfo.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnThem)
						.addComponent(btnSua)
						.addComponent(btnXoa))
					.addContainerGap(281, Short.MAX_VALUE))
		);

		dtmTraCuu = new DefaultTableModel();
		dtmTraCuu.addColumn("ID");
		dtmTraCuu.addColumn("Tên vật tư");
		dtmTraCuu.addColumn("Đơn vị đo");
		dtmTraCuu.addColumn("Nhà cung cấp");

		pnInfo.setLayout(gl_pnInfo);

		JPanel pnTable = new JPanel();
		pnTable.setBackground(new Color(255, 255, 255));
		add(pnTable);
		pnTable.setLayout(new BorderLayout(0, 0));
		tblTraCuu = new JTable(dtmTraCuu);


		JScrollPane scrollTblTraCuu = new JScrollPane(tblTraCuu,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		pnTable.add(scrollTblTraCuu, BorderLayout.CENTER);
	}
}
