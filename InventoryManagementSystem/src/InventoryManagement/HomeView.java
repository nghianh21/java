/* 
 * class HomeView
 * created by Nguyen Huu Nghia
 * 
 */
package InventoryManagement;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.BoxLayout;
import java.awt.FlowLayout;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JSeparator;
import java.awt.Component;
import javax.swing.JFormattedTextField;
import java.awt.CardLayout;
import java.awt.GridLayout;
import java.awt.SystemColor;
import java.sql.Connection;
import java.sql.DriverManager;

import net.miginfocom.swing.MigLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.GridBagLayout;
import javax.swing.SwingConstants;
import java.awt.Font;
import com.toedter.calendar.JDateChooser;

import InventoryManagement.View.PanelCustomer;
import InventoryManagement.View.PanelHome;
import InventoryManagement.View.PanelInput;
import InventoryManagement.View.PanelObject;
import InventoryManagement.View.PanelOutput;
import InventoryManagement.View.PanelSuplier;
import InventoryManagement.View.PanelTraCuu;
import InventoryManagement.View.PanelUnit;
import InventoryManagement.View.PanelUser;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import java.awt.GridBagConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import java.awt.event.ActionListener;
import java.awt.Panel;
import java.awt.Toolkit;
import javax.swing.Icon;
import java.awt.Window.Type;

public class HomeView {

	private JFrame frame;
	private JTable tblTraCuuTonKho;
	private DefaultTableModel dtmTraCuuTonKho;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@SuppressWarnings("unused")
			public void run() {
				try {
//					Connection conn = getConnection("TAB21", "InventoryManagement");
					HomeView window = new HomeView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public HomeView() {
		initialize();
	}
	
	/**
	 * Connect SQL Server
	 */
//	public static Connection getConnection(String strServer, String strDatabase) {
//		Connection conn = null;
//		try {
//			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
//			String dbURL = "jdbc:sqlserver://"+strServer+":1433;databaseName="+strDatabase+";integratedSecurity=true;";
//			conn = DriverManager.getConnection(dbURL);
//			System.out.println("connect successfully!");
//		} catch (Exception ex) {
//			System.out.println("connect failure!");
//			ex.printStackTrace();
//		}
//		return conn;
//	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Quản lý kho");
		frame.getContentPane().setBackground(new Color(255, 255, 255));
		frame.setForeground(Color.GRAY);
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Admin\\Documents\\java\\InventoryManagementSystem\\Icon\\inventory.png"));
		frame.setResizable(false);
		// set full screen
//		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);  
		
//		frame.setUndecorated(true);
		frame.setBounds(100, 100, 1200, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.X_AXIS));
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.LEFT);
		tabbedPane.setForeground(Color.GRAY);
		tabbedPane.setFont(new Font("Tahoma", Font.BOLD, 13));
		tabbedPane.setBackground(new Color(255, 255, 255));
		tabbedPane.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				// TODO Auto-generated method stub
				System.out.println("Tab: " + tabbedPane.getSelectedIndex());
			}
		});

		frame.getContentPane().add(tabbedPane);
		
		JPanel pnTabHome = new PanelHome();
		pnTabHome.setBackground(Color.WHITE);
		tabbedPane.addTab("Tổng quan     ", new ImageIcon("C:\\Users\\Admin\\Documents\\java\\InventoryManagementSystem\\Icon\\overview-01.png"), pnTabHome, null);
		
		JPanel pnTabTraCuu = new PanelTraCuu();
		tabbedPane.addTab("Tra Cứu         ", new ImageIcon("C:\\Users\\Admin\\Documents\\java\\InventoryManagementSystem\\Icon\\nghiakung-01.png"), pnTabTraCuu, null);
		
		JPanel pnTabNhapKho = new PanelInput();
		tabbedPane.addTab("Nhập Kho       ", new ImageIcon("C:\\Users\\Admin\\Documents\\java\\InventoryManagementSystem\\Icon\\import-01.png"), pnTabNhapKho, null);
		
		JPanel pnTabXuatKho = new PanelOutput();
		tabbedPane.addTab("Xuất Kho        ", new ImageIcon("C:\\Users\\Admin\\Documents\\java\\InventoryManagementSystem\\Icon\\xuat-01.png"), pnTabXuatKho, null);
		
		JPanel pnTabObject = new PanelObject();
		tabbedPane.addTab("Vật Tư           ", new ImageIcon("C:\\Users\\Admin\\Documents\\java\\InventoryManagementSystem\\Icon\\vatlieu-01.png"), pnTabObject, null);
		
		JPanel pnUnit = new PanelUnit();
		tabbedPane.addTab("Đơn Vị Tính    ", new ImageIcon("C:\\Users\\Admin\\Documents\\java\\InventoryManagementSystem\\Icon\\unit-01.png"), pnUnit, null);
		
		JPanel pnTabNhaCungCap = new PanelSuplier();
		tabbedPane.addTab("Nhà Cung Cấp", new ImageIcon("C:\\Users\\Admin\\Documents\\java\\InventoryManagementSystem\\Icon\\supliter-01.png"), pnTabNhaCungCap, null);
	
		JPanel pnTabKhachhang = new PanelCustomer();
		tabbedPane.addTab("Khách Hàng   ", new ImageIcon("C:\\Users\\Admin\\Documents\\java\\InventoryManagementSystem\\Icon\\khachhang-01.png"), pnTabKhachhang, null);
		
		JPanel pnUser = new PanelUser();
		tabbedPane.addTab("Người Dùng   ", new ImageIcon("C:\\Users\\Admin\\Documents\\java\\InventoryManagementSystem\\Icon\\user-01.png"), pnUser, null);
	
	}
}

