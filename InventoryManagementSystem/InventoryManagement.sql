﻿create database InventoryManagement 
go
use InventoryManagement 
go



create table UserRole
(
	ID int identity(1,1) primary key,
	DisplayName nvarchar(max),
	isDeleted TINYINT	 -- đánh dấu, 1 là xóa
)
go

insert into UserRole(DisplayName) values(N'Admin')
insert into UserRole(DisplayName) values(N'Staff')

create table Users
(
	ID int identity(1,1) primary key,
	DisplayName nvarchar(max),
	UserName nvarchar(100),
	Password nvarchar(max),
	IDRole int not null,
	isDeleted TINYINT	 -- đánh dấu, 1 là xóa

	foreign key (IdRole) references UserRole(id)
)
go

insert into Users(DisplayName, UserName, Password, IdRole) values(N'Huu Nghia', N'admin', N'db69fc039dcbd2962cb4d28f5891aae1',1)
insert into Users(DisplayName, UserName, Password, IdRole) values(N'Staff', N'staff', N'978aae9bb6bee8fb75de3e4830a1be46',2)

create table Subinventory
(
	ID int identity(1,1) primary key,
	DisplayName nvarchar(max),
	DisplayAddress nvarchar(max),
	isDeleted TINYINT	 -- đánh dấu, 1 là xóa
)
go

create table Unit
(
	ID int identity(1,1) primary key,
	DisplayName nvarchar(max),
	isDeleted TINYINT	 -- đánh dấu, 1 là xóa
)
go

create table Supplier
(
	ID int identity (1,1) primary key,
	DisplayName nvarchar(max),
	Address nvarchar(max),
	Phone nvarchar(20),
	Email nvarchar(200),
	isDeleted TINYINT	 -- đánh dấu, 1 là xóa
)
go

create table Customer
(
	ID int identity (1,1) primary key,
	DisplayName nvarchar(max),
	Address nvarchar(max),
	Phone nvarchar(20),
	Email nvarchar(200),
	isDeleted TINYINT	 -- đánh dấu, 1 là xóa
)
go

create table Object
(
	ID nvarchar(128) primary key,
	DisplayName nvarchar(max),
	IDUnit int not null,
	IDSupplier int not null,
	isDeleted TINYINT	 -- đánh dấu, 1 là xóa

	foreign key(IdUnit) references Unit(id),
	foreign key(IdSuplier) references Suplier(id)

)
go

create table Inputs
(
	ID nvarchar(128) primary key,
	DateInput DateTime,
	isDeleted TINYINT	 -- đánh dấu, 1 là xóa
)
go

create table InputInfo 
(
	ID nvarchar(128) primary key,
	IDObject nvarchar(128) not null,
	IDInput nvarchar(128) not null,
	Amount int,
	InputPrice float default 0,
	OutputPrice float default 0,
	InputStatus nvarchar(max),
	isDeleted TINYINT	 -- đánh dấu, 1 là xóa

	foreign key (idObject) references Object(id),
	foreign key (idInput) references Inputs(id)
)
go

create table Outputs
(
	ID nvarchar(128) primary key,
	DateOutput DateTime,
	isDeleted TINYINT -- đánh dấu, 1 là xóa
)
go

create table OutputInfo 
(
	ID nvarchar(128) primary key,
	IdOutput nvarchar(128) not null,
	IdObject nvarchar(128) not null, 
	IdInputInfo nvarchar(128) not null,
	Amount Int,
	IdCustomer int not null,
	OutputStatus nvarchar(max),
	isDeleted TINYINT	 -- đánh dấu, 1 là xóa

	foreign key (idOutput) references Outputs(id),
	foreign key (idObject) references Object(id),
	foreign key (idInputInfo) references Inputs(id),
	foreign key (idCustomer) references Customer(id)
)
go